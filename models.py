#coding: utf-8
import datetime
import hashlib
import bcrypt
from main import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(255), nullable=True)
    token = db.Column(db.String(255), nullable=True)
    password = db.Column(db.String(255), nullable=True)
    oauth_id = db.Column(db.Integer, nullable=True)

    def __unicode__(self):
        return u'%s' % self.email

    @staticmethod
    def hash_password(password):
        return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

    def set_password(self, password):
        self.password = self.hash_password(password)

    def check_password(self, password):
        return bcrypt.hashpw(password.encode('utf-8'), self.password) == self.password


class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    session_key = db.Column(db.String(255), unique=True)
    id_user = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    date_add = db.Column(db.DateTime, nullable=False)

    user = db.relationship(User)

    def __unicode__(self):
        return u'%s' % self.id

    @staticmethod
    def create_session_key(id_user):
        return hashlib.sha512(u'%s-%i' % (datetime.datetime.now(), id_user)).hexdigest()
