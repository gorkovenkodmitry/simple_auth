activate_this = 'env_path/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
import os
connection = os.path.dirname(__file__)
if not connection in sys.path:
	sys.path.insert(0, connection)
from main import app as application
