from flask_wtf import Form
from sqlalchemy.orm.exc import NoResultFound
from wtforms import StringField, IntegerField, SelectField
from wtforms.validators import InputRequired, Email, Optional
from main import db
import models


auth_types = ['simple', 'facebook']


class AuthSignupForm(Form):
    type = SelectField('type', validators=[InputRequired()], choices=[(x, x) for x in auth_types])
    email = StringField('email', validators=[InputRequired(), Email()])
    password = StringField('password')
    facebook_id = IntegerField('facebook_id', validators=[Optional()])
    facebook_token = StringField('facebook_token', validators=[Optional()])

    def validate_on_submit(self):
        success = Form.validate_on_submit(self)
        if self.type.data == 'simple':
            if not self.password.data:
                success = False
                self.errors['password'] = [u'This field is required.']
            if success:
                pass
        if self.type.data == 'facebook':
            if not self.facebook_id.data:
                success = False
                self.errors['facebook_id'] = [u'This field is required.']
            if not self.facebook_token.data:
                success = False
                self.errors['facebook_token'] = [u'This field is required.']
        return success


class AuthSigninForm(Form):
    type = SelectField('type', validators=[InputRequired()], choices=[(x, x) for x in auth_types])
    email = StringField('email', validators=[Optional(), Email()])
    password = StringField('password')
    facebook_token = StringField('facebook_token', validators=[Optional()])

    @property
    def user(self):
        try:
            return self._user
        except AttributeError:
            return None

    def validate_on_submit(self):
        success = Form.validate_on_submit(self)
        if self.type.data == 'simple':
            if not self.password.data:
                success = False
                self.errors['password'] = [u'This field is required.']
            else:
                try:
                    self._user = db.session.query(models.User).filter(
                        models.User.type == self.type.data,
                        models.User.email == self.email.data
                    ).one()
                    if not self.user.check_password(self.password.data):
                        success = False
                        self.errors['password'] = [u'Wrong password']
                except NoResultFound:
                    success = False
                    self.errors['email'] = [u'User not found']
        if self.type.data == 'facebook':
            if not self.facebook_token.data:
                success = False
                self.errors['facebook_token'] = [u'This field is required.']
            else:
                try:
                    self._user = db.session.query(models.User).filter(
                        models.User.type == self.type.data,
                        models.User.token == self.facebook_token.data
                    ).one()
                except NoResultFound:
                    success = False
                    self.errors['email'] = [u'User not found']
        return success
