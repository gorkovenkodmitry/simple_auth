#coding: utf-8
import datetime
from flask import Flask, render_template, jsonify
from sqlalchemy.orm.exc import NoResultFound
from config.config import STATIC_PATH, SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, SQLALCHEMY_TRACK_MODIFICATIONS
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__, static_folder=STATIC_PATH)
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_MIGRATE_REPO'] = SQLALCHEMY_MIGRATE_REPO
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_TRACK_MODIFICATIONS
app.secret_key = '\xa1\xe8q6\xd9\xfb\x95\xb0\xb1a\xe4\xe8\x8c-z\x7f\x87\xfb?\xbb\x9c\x13Y\x10'
db = SQLAlchemy(app)

import models
import forms


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/auth/signup/', methods=['POST'])
def auth_signup():
    form = forms.AuthSignupForm(csrf_enabled=False)
    if form.validate_on_submit():
        if form.type.data == 'simple':
            try:
                user = db.session.query(models.User).filter(
                    models.User.type == form.type.data,
                    models.User.email == form.email.data
                ).one()
            except NoResultFound:
                user = models.User(
                    type=form.type.data,
                    email=form.email.data
                )
                db.session.add(user)
            user.set_password(form.password.data)

        elif form.type.data == 'facebook':
            try:
                user = db.session.query(models.User).filter(
                    models.User.type == form.type.data,
                    models.User.token == form.facebook_token.data
                ).one()
            except NoResultFound:
                user = models.User(
                    type=form.type.data,
                    token=form.facebook_token.data
                )
                db.session.add(user)
            user.email = form.email.data
            user.oauth_id = form.facebook_id.data
        db.session.commit()
        return '', 200
    return jsonify({'errors': form.errors}), 400


@app.route('/auth/signin/', methods=['POST'])
def auth_signin():
    form = forms.AuthSigninForm(csrf_enabled=False)
    if form.validate_on_submit():
        new_session = models.Session(
            id_user=form.user.id,
            date_add=datetime.datetime.now(),
            session_key=models.Session.create_session_key(form.user.id)
        )
        db.session.add(new_session)
        db.session.commit()
        return new_session.session_key, 200
    return jsonify({'errors': form.errors}), 400

if __name__ == "__main__":
    app.run(debug=True, port=5005)
