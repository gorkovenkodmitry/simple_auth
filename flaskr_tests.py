import main as flaskr
import unittest

class FlaskTestCase(unittest.TestCase):

    def setUp(self):
        flaskr.app.config['TESTING'] = True
        self.app = flaskr.app.test_client()

    def auth_signup_simple(self, email, password):
        return self.app.post(
            '/auth/signup/',
            data=dict(type='simple', email=email, password=password),
            follow_redirects=True
        )

    def auth_signin_simple(self, email, password):
        return self.app.post(
            '/auth/signin/',
            data=dict(type='simple', email=email, password=password),
            follow_redirects=True
        )

    def test_signup_simple(self):
        rv = self.auth_signup_simple('mail@admin.com', 'default')
        assert 'errors' not in rv.data
        rv = self.auth_signup_simple('mail@admin.com', '')
        assert 'errors' in rv.data

    def test_signin_simple(self):
        rv = self.auth_signin_simple('mail@admin.com', 'default')
        assert 'errors' not in rv.data
        rv = self.auth_signin_simple('mail@admin.com', 'default1')
        assert 'errors' in rv.data
        rv = self.auth_signin_simple('mail@admin.com', '')
        assert 'errors' in rv.data

    def auth_signup_fb(self, email, facebook_id, facebook_token):
        return self.app.post(
            '/auth/signup/',
            data=dict(type='facebook', email=email, facebook_id=facebook_id, facebook_token=facebook_token),
            follow_redirects=True
        )

    def auth_signin_fb(self, facebook_token):
        return self.app.post(
            '/auth/signin/',
            data=dict(type='facebook', facebook_token=facebook_token),
            follow_redirects=True
        )

    def test_signup_fb(self):
        rv = self.auth_signup_fb('mail1@admin.com', '-1', 'abc')
        assert 'errors' not in rv.data
        rv = self.auth_signup_fb('mail1@admin.com', '', '')
        assert 'errors' in rv.data

    def test_signin_fb(self):
        rv = self.auth_signin_fb('abc')
        assert 'errors' not in rv.data
        rv = self.auth_signin_fb('')
        assert 'errors' in rv.data

if __name__ == '__main__':
    unittest.main()
